import bcrypt from 'bcrypt'
import { redirect, fail } from '@sveltejs/kit';
import { supabase } from "$lib/supabaseClient";
import { CookieSession } from 'svelte-kit-cookie-session/core';



/**@type {import('./$types').Actions} */
export const actions = {
    login: async ({ locals, cookies, url, request }) => 
    {

        //Получим данные формы
        const formdata = await request.formData()
        let usernamemailphone = formdata.get("username")
        let password = formdata.get('password')
        let date2 = new Date().toISOString().slice(0, 10)

        console.log("mysession:",locals.session.data)

        //Проверим, есть ли пользователь по логину
        let user = await supabase
            .from('user')
            .select()
            .eq('username', usernamemailphone)

        //Получаем токен для обновления пользователя
        let token = crypto.randomUUID()

        //Такого пользователя по логину нет
        if (!user.data.length) 
        {
            //Проверим,есть ли пользователь по почте
            let user2 = await supabase
                .from('user')
                .select()
                .eq('email',usernamemailphone)
            
            //Пользователя по почте нет
            if(!user2.data.length)
            {
                //Проверим пользователя по номеру телефона
                let user3 = await supabase
                .from('user')
                .select()
                .eq('telephone',usernamemailphone)
                

                //Пользователя по номеру телефона нет
                if(!user3.data.length)
                {
                    return fail(400, {wrongdata: true})
                }
                else
                {
                    const checkPassword3 = await bcrypt.compare(password, user3.data[0].passwordHash)
                    if(!checkPassword3)
                    {
                        return fail(400, { bad_pwd: true })
                    }
                    const upd_user = await supabase
                    .from('user')
                    .update({ userAuthToken: token, updated_at: date2})
                    .eq('User_ID', user3.data[0].User_ID)
        
                    //Нет ошибок при update
                    if (!upd_user.error) 
                    {
                        await locals.session.set({ user: { id: user3.data[0].id, name: user3.data[0].username, token: token }, views: 0, data1: [], data2: [] });
                        throw redirect(303, '/')
                    } 
                    //Есть ошибки
                    else 
                    {
                        throw redirect(300,'/login')
                    }
                }
            }
            else
            {
                const checkPassword2 = await bcrypt.compare(password, user2.data[0].passwordHash)
                if(!checkPassword2)
                {
                    return fail(400, { bad_pwd: true })
                }
                const upd_user = await supabase
                .from('user')
                .update({ userAuthToken: token, updated_at: date2})
                .eq('User_ID', user2.data[0].User_ID)
    
                //Нет ошибок при update
                if (!upd_user.error) 
                {
                    await locals.session.set({ user: { id: user2.data[0].id, name: user2.data[0].username, token: token }, views: 0, data1: [], data2: [] });
                    throw redirect(303, '/')
                } 
                //Есть ошибки
                else 
                {
                    throw redirect(300,'/login')
                }
            }
        }
        else
        {
            //Если пароль не прошёл проверку
            const checkPassword = await bcrypt.compare(password, user.data[0].passwordHash)
            if(!checkPassword)
            {
                return fail(400, { bad_pwd: true })
            }
            const upd_user = await supabase
            .from('user')
            .update({ userAuthToken: token, updated_at: date2})
            .eq('User_ID', user.data[0].User_ID)

            //Нет ошибок при update
            if (!upd_user.error) 
            {
                await locals.session.set({ user: { id: user.data[0].id, name: user.data[0].username, token: token }, views: 0, data1: [], data2: [] });
                throw redirect(303, '/')
            } 
            //Есть ошибки
            else 
            {
                throw redirect(300,'/login')
            }
        }
    }
}