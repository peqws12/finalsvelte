import { supabase } from "$lib/supabaseClient";

export async function load() {
  const { data } = await supabase.from("todousers").select();
  return {
    todousers: data ?? [],
  };
}