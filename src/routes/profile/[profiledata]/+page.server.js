import { error, fail, redirect } from '@sveltejs/kit';
import { supabase } from "$lib/supabaseClient";
import { not_equal } from 'svelte/internal';

/**@type {import('./$types').PageServerLoad} */
export async function load({ locals }) {
    const tektoken = locals.session.data.user.token;
    const { data } = await supabase
        .from('user')
        .select()
        .eq('userAuthToken', tektoken);


    return {
        profiledata: data ?? [],
        userFIO: data[0].FIO ?? [],
        username: data[0].username ?? [],
        email: data[0].email ?? [],
        telephone: data[0].telephone ?? []

    }
};

/**@type {import('./$types').Actions} */
export const actions = {
    changeuserdata: async ({ locals, cookies, url, request }) => {

        //Получим все данные с формы
        const formdata = await request.formData()
        let oldFIO = formdata.get("olduserFIO")
        let oldusername = formdata.get("oldusername")
        let oldtelephone = formdata.get("oldtelephone")
        let oldemail = formdata.get("oldemail")
        let newFIO = formdata.get("newuserFIO")
        let newusername = formdata.get("newusername")
        let newtelephone = formdata.get("newtelephone")
        let newemail = formdata.get("newemail")
        console.log("newusername is ", newusername)

        let tektoken = locals.session.data.user.token


        if (!newFIO.length || !newusername.length || !newtelephone.length || !newemail?.length) {
            console.log("Зашло в проверку на null");
            throw redirect(300, '/')
        }
        // .eq("username.eq.${newusername},email.eq.${newemail},telephone.eq.${newtelephone}")

        let userupdate = await supabase
            .from('user')
            .update([
                {
                    username: newusername,
                    email: newemail,
                    telephone: newtelephone,
                    FIO: newFIO
                }
            ])
            .eq('userAuthToken', tektoken)
            .select()

        if (!userupdate.error) {
            let token = crypto.randomUUID()
            locals.session.destroy()
        }
        else {
            console.log("error")
        }
    }

}
