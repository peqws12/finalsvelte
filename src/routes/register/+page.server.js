import bcrypt from 'bcrypt'
import { fail, redirect } from '@sveltejs/kit';
import { supabase } from "$lib/supabaseClient";
import { CookieSession } from 'svelte-kit-cookie-session/core';
import { not_equal } from 'svelte/internal';

/**@type {import('./$types').Actions} */
export const actions = {
    register: async ({ locals, cookies, url, request }) => {

        //Получим данные формы
        const formdata = await request.formData()
        let username = formdata.get("username")
        let password = formdata.get('password')
        let useremail = formdata.get('email')
        let userphone = formdata.get('phone')
        let FIOUser = formdata.get('FIO')
        console.log("username: ",username, typeof(username))
        console.log("password: ",password, typeof(username))
        console.log("useremail: ",useremail, typeof(useremail))
        console.log("userphone: ",userphone, typeof(userphone))
        console.log("FIOUser: ",FIOUser, typeof(FIOUser))
        if(username?.length == 0 || password?.length == 0 || useremail?.length == 0 || userphone?.length == 0)
        {
            return fail(409, { user: true })
        }
        //получим ID роли USER
        let { data, error } = await supabase
        .from('roles')
        .select('role_id')
        .eq('name','USER')

        let user_role_id = data[0].role_id
        console.log("userroleid: ",user_role_id)
        //Не получилось получить id роли User
        if(user_role_id.length)
        {
            return fail(409, { user: true })
        }



        //Проверим, нет ли уже такого польз-ля
        let user = await supabase
            .from('user')
            .select('username')
            .eq('username', username)

        //Такой польз-ль уже есть в системе
        if (user.data.length)
        {
            return fail(409, { user: true })
        }
        //Проверим,есть ли такая почта
        let useremaildata = await supabase
            .from('user')
            .select('email')
            .eq('email',useremail)

        //Такая почта уже зарезервирована
        if(useremaildata.data?.length)
        {
            return fail(409, { user: true})
        }

        //Проверяем,использовали ли такой номер телефона
        let userphonedata = await supabase
            .from('user')
            .select('phone')
            .eq('phone',userphone)

        //Такой номер уже использует пользователь
        if(userphonedata.data?.length)
        {
            return fail(409, {user:true})
        }
        //Создадим польз-ля с ролью USER
        if (user_role_id != null) 
        {
            const new_user = await supabase
            .from('user')
            .insert(
                {
                    username: username,
                    email:useremail,
                    FIO:FIOUser,
                    telephone:userphone,
                    passwordHash: await bcrypt.hash(password, 10),
                    userAuthToken: crypto.randomUUID(),
                    role_id: user_role_id
                }
            )
            .select() 

            console.log(new_user.data)
            //Проверяем,создался ли пользователей
            if(new_user.data != null)
            {
                console.log("Пользователь создался!!");
                throw redirect(303, '/login')   
            } 
            else
            {
                return fail(409, { user: true })     
            }
        } 
        else 
        {
            return fail(409, { user: true })     
        }
    }

}