//!!!!!!!!! https://www.npmjs.com/package/svelte-kit-cookie-session
//!!!!!!!!! https://github.com/pixelmund/svelte-kit-cookie-session#setting-the-session
//!!!!!!!!!!https://github.com/sveltejs/kit/discussions/5883
//===========================================================

//https://supabase.com/docs/guides/getting-started/quickstarts/sveltekit
//https://supabase.com/docs/reference/javascript/order
// https://supabase.com/docs/reference/javascript/update
//https://app.supabase.com/project/nekgixtskpgyeeymvcel/settings/api

import { fail, redirect } from '@sveltejs/kit';
import { supabase } from "$lib/supabaseClient";
import { not_equal } from 'svelte/internal';

/**@type {import('./$types').PageServerLoad} */
export async function load({ locals }) {
    const tektoken = locals.session.data.user.token
    
    let tekuser = await supabase
    .from('user')
    .select('User_ID,role_id')
    .eq('userAuthToken',tektoken)


    const myuserid = tekuser.data[0].User_ID
    const myroleid = tekuser.data[0].role_id

    let userRole = await supabase
        .from('NewRoles')
        .select('name_role')
        .eq('myrole_id',myroleid)
    

    const userrolename = userRole.data[0].name_role
    if (userrolename === "USER") {
        const { data } = await supabase
        .from("todousers")
        .select()
        .eq('User_ID',myuserid) 
        .order('ToDo_ID', { ascending: false })


    return {
        todousers: data ?? [],
        prava : userRole.data[0] ?? [],
    }
    } 
    else 
    {
        const { data } = await supabase
        .from("todousers")
        .select()
        .order('ToDo_ID')

    return {
        todousers: data ?? [],
        prava : userRole.data[0].name_role ?? [],
    }
    }
};



/**@type {import('./$types').Actions} */
export const actions = {
    changetodo: async ({ locals, cookies, url, request }) => {
        const data = await request.formData()

        const tektoken = locals.session.data.user.token
    
        let tekuser = await supabase
        .from('user')
        .select('User_ID,role_id')
        .eq('userAuthToken',tektoken)
    
    
        const myuserid = tekuser.data[0].User_ID

        const myrole_id = tekuser.data[0].role_id

        let prava = await supabase
        .from('NewRoles')
        .select('name_role')
        .eq('myrole_id',myrole_id)

        let Task = data.get("Task")
        let id = data.get('id')
        let dateTask = data.get('dueDate')
        let marking = data.get('marking')

        console.log(data)
        if(!marking?.length)
        {
            console.log("поменяли на ложь");
            marking = false;
        }
        console.log("marking is ",marking)
        let nowDate = Date.now()
        console.log('id todo: ',id)
        if(id != null)
        {
            console.log("!!!!!! id founded")
            const { error } = await supabase
            .from('todousers')
            .update({ Task : Task,DueDate : dateTask,MarkingForDeletion : marking})
            .eq('ToDo_ID', id)

            if (error) {
                return fail(parseInt(error.code), { error: error.message });
            }

        }
        else
        {
            let currentDate = new Date()
            const day = String(currentDate.getDate()).padStart(2, '0');

            // Форматируем месяц
            const month = String(currentDate.getMonth() + 1).padStart(2, '0');
        
            // Форматируем год
            const year = String(currentDate.getFullYear());
        
            // Форматируем часы
            const hours = String(currentDate.getHours()).padStart(2, '0');
        
            // Форматируем минуты
            const minutes = String(currentDate.getMinutes()).padStart(2, '0');
        
            // Создаем строку даты в нужном формате "yyyy-MM-dd'T'HH:mm"
            const formattedDate = `${year}-${month}-${day}T${hours}:${minutes}`;

            const { error } = await supabase
            .from('todousers')
            .insert([
                {User_ID : myuserid,
                Task : Task,
                DateCreation : formattedDate,
                DueDate : dateTask,
                MarkingForDeletion: false
                }
            ])
            if (error) {
                return fail(parseInt(error.code), { error: error.message });
            }
        }
    },

    delete_todo : async ({locals, cookies, url, request}) =>{
        const data = await request.formData()
        let id = data.get('id')
        console.log("id is ",id)
        const tektoken = locals.session.data.user.token
    
        let tekuser = await supabase
        .from('user')
        .select('role_id')
        .eq('userAuthToken',tektoken)
    
    
        const myrole_id = tekuser.data[0].role_id

        let prava = await supabase
        .from('NewRoles')
        .select('name_role')
        .eq('myrole_id',myrole_id)

        
        if (id != null && prava.data[0].name_role != "ADMINISTATOR") 
        {
            const { error } = await supabase
            .from('todousers')
            .delete()
            .eq('ToDo_ID', id)
            if (error) {
                return fail(parseInt(error.code), { error: error.message });
            }
        } 
    }
}

    // check: async ({ locals, cookies, url, request, params }) => {

    //     const { data1 = [] } = locals.session.data;
    //     // await locals.session.set({ views: 222, data1: [], data2: [] });

    //     const data = await request.formData()
    //     let answer = data.get("answer")

    //     //стандартная робота с cookis
    //     let cook_adder = cookies.get('adder')
    //     cookies.set('adder', (parseInt(answer) + parseInt(cook_adder)).toString(10))

    //     //работа с состоянием через url-параметры
    //     let summ = url.searchParams.get('/check')
    //     let res = parseInt(summ) + parseInt(answer);


    //     if (parseInt(answer) == 100) {
    //         const { data1 = [] } = locals.session.data;
    //         if (data1.length == 0) {
    //             await locals.session.set({ views: 222, data1: [-321], data2: [] });
    //         } else {
    //             await locals.session.update((data) => ({ data1: [...data.data1, 5, 6, 7] }))

    //             //BAD!! await locals.session.update(({ data1 }) => [...data1, 333, 444])
    //         }
    //     }


    //     try {
    //         if (parseInt(answer) == 200) {
    //             await locals.session.update((data) => ({
    //                 user: { name: "qq", email: "qq@ww.com" },
    //                 data2: [...data.data2, 'qq', 'ww', 'ee'],
    //                 data1: [...data.data1, 555, 666, 777],
    //             }));
    //         }
    //     } catch (err) {
    //         console.log(err)
    //     }
    //     await locals.session.refresh(10)

    //     return { summa: 1000, text: '<h1>ТЕКСТ</h1>' }
    // }
