import bcrypt from 'bcrypt'
import { redirect, fail } from '@sveltejs/kit';
import { supabase } from "$lib/supabaseClient";


/**@type {import('./$types').Actions} */
export const actions = {
    findlogin: async ({ locals, cookies, url, request }) => {
        const dataForm = await request.formData()
        let login = dataForm.get("username")

        console.log(login)
        let  data  = await supabase
            .from('user')
            .select()
            .eq('username', login)
        
        if (!data.data?.length) {

            return fail(400, { loginfound: false })
        }
        else {
            console.log(data)
            let data2 = await supabase
                .from('user')
                .select()
                .eq('username',login)
            
            console.log(data2.statusText)
            console.log(data2)
            if(data2.data?.length)
            {
                return fail(400,{loginfound: true,loginis: data2.data[0].username})
            }
            else
            {
                return fail(400,{loginfound:false})
            }
        }
    },
    resetpassword: async ({ locals, cookies, url, request }) => {
        const dataForm = await request.formData()
        console.log(dataForm)
        let login = dataForm.get("newlogin")

        let newpassword = dataForm.get("password2")
        let newpasswordhash = await bcrypt.hash(newpassword, 10)
        console.log(newpassword)
        console.log(login)
        let resultchange = await supabase
            .from('user')
            .update({passwordHash : newpasswordhash })
            .eq('username',login)
            .select()
        if(!resultchange.data?.length)
        {
            if(login == null)
            {
                console.log("notsup")
                return fail(400,{loginfound : false})
            }
            else
            {
                console.log("notsup")
                return fail(400,{loginfound : true,loginis : login})
            }
        }
        else
        {
            console.log("sup")
            throw redirect(303,"/login")
        }

    }

}